// ############# BEGIN FILE VARIABLE.CPP ############ //
#ifndef LIBREFLEXION_MEMBERS_VARIABLE_CPP
#define LIBREFLEXION_MEMBERS_VARIABLE_CPP

// ****************** DEPENDANCES ******************* //

#include "Variable.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ################ BEGIN CLASS GVARIABLE ############### //

		// ****************** CONSTRUCTOR ******************* //

		Variable::Variable ( const string&  name )
		: AnyPointer ( name )
		{}
		
		Variable::Variable ( Variable&  variable )
		: AnyPointer( variable )
		{}
		
		// ****************** DESTRUCTOR ******************** //

		Variable::~Variable ( void )
		{}

		// ******************** GETTERS ********************* //

		string
		Variable::Description ( void ) const
		{
			stringstream  output;
			output << "Variable : [name= " << this->Name() << ", value= " << this->StrValue() << "]";
			return output.str();
		}

		// ******************* OPERATORS ******************** //

		Variable&
		Variable::operator= ( Variable&  variable )
		{
			this->AnyPointer::operator=( static_cast< AnyPointer& >( variable ) );
			return (*this);
		}

		// ############### END CLASS GVARIABLE ################## //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_VARIABLE_HPP //
// ############## END FILE VARIABLE.HPP ############# //