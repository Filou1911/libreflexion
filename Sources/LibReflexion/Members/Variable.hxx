// ############# BEGIN FILE VARIABLE.HXX ############# //
#ifndef LIBREFLEXION_MEMBERS_VARIABLE_HXX
#define LIBREFLEXION_MEMBERS_VARIABLE_HXX

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ############ BEGIN CLASS GVARIABLE ############### //

		template< typename Type >
		Variable::Variable ( const string&  name, Type* ptrData )
		: AnyPointer( name, ptrData )
		{}

		// ******************** GETTERS ********************* //

		template< typename Type >
		Type& 
		Variable::Value ( void )
		{
			return this->AnyPointer::Value< Type >();
		}
		
		// ******************* OPERATORS ******************** //

		template< typename Type >
		Variable&
		Variable::operator= ( Type  value )
		{
			this->AnyPointer::operator=( value );
			return (*this);
		}


		template< typename Type >
		Variable::operator Type ()
		{
			return this->Value< Type >();
		}


		// ############### END CLASS VARIABLE ############### //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_VARIABLE_HXX //
// ############# END FILE VARIABLE.HXX ############## //