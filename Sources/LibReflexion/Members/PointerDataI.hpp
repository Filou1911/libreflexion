// ########### BEGIN FILE POINTERDATAI.HPP ########## //
#ifndef LIBREFLEXION_MEMBERS_POINTERDATAI_HPP
#define LIBREFLEXION_MEMBERS_POINTERDATAI_HPP

// ****************** DEPENDANCES ******************* //

#include "LibDependances.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //

	namespace Members {

		// ############ BEGIN CLASS POINTERDATAI ######### //

		class PointerDataI {

			// ****************** DESTRUCTOR ******************** //
		public:
			virtual ~PointerDataI ( void );

			// ******************** GETTERS ********************* //
		public:
			virtual void*			Address 	( void ) = 0;
			virtual PointerDataI*	Clone		( void ) = 0;
			virtual string			StrAddress	( void ) const = 0;
			virtual string			StrValue	( void ) const = 0;

		};
		// ############### END CLASS POINTERDATAI ########### //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_POINTERDATAI_HPP //
// ############ END FILE POINTERDATAI.HPP ########### //