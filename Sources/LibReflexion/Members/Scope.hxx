// ############## BEGIN FILE SCOPE.HXX ############## //
#ifndef LIBREFLEXION_MEMBERS_SCOPE_HXX
#define LIBREFLEXION_MEMBERS_SCOPE_HXX

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ############### BEGIN CLASS SCOPE ################ //
		
		// ****************** CONSTRUCTOR ******************* //

		template < typename... ArgsTypes >
		Scope::Scope ( Scope* ptrParent, const string&  name, ArgsTypes... members )
		: Member( name ), ptrParent( ptrParent )
		{
			this->AddMembers( members... );
		}

		// ******************** SETTERS ********************* //

		template < typename... ArgsTypes >
		bool
		Scope::AddMembers ( Member* ptrMember, ArgsTypes... rest )
		{
			this->AddMember( ptrMember );

			return this->AddMembers( rest... );
		}


		// ################ END CLASS SCOPE ################# //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_SCOPE_HXX //
// ############### END FILE SCOPE.HXX ############### //