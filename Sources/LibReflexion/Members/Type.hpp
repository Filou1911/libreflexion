// ############## BEGIN FILE TYPE.HPP ############### //
#ifndef LIBREFLEXION_MEMBERS_TYPE_HPP
#define LIBREFLEXION_MEMBERS_TYPE_HPP

// ****************** DEPENDANCES ******************* //

// LOCAL DEPENDANCES : //
#include "Variable.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ################ BEGIN CLASS TYPE ################ //

		class Type : public Member {

			// ******************** MEMBERS ********************* //
		private:
			UInt size;

			// ****************** CONSTRUCTOR ******************* //
		public:
			Type ( const string& name, UInt size );

			// ****************** DESTRUCTOR ******************** //

			virtual ~Type ( void );

			// ******************** GETTERS ********************* //

			using Member::Name;

			virtual UInt Size ( void ) const;

			// ******************* UTILITIES ******************** //

			//virtual Variable Allocate ( void ) const = 0;

		};
		// ################# END CLASS TYPE ################# //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_TYPE_HPP //
// ################ END FILE TYPE.HPP ############### //