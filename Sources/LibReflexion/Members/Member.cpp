// ############# BEGIN FILE MEMBER.CPP ############## //
#ifndef LIBREFLEXION_MEMBERS_MEMBER_CPP
#define LIBREFLEXION_MEMBERS_MEMBER_CPP

// ****************** DEPENDANCES ******************* //

#include "Member.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ############## BEGIN CLASS MEMBER ################ //

		// ****************** CONSTRUCTOR ******************* //

		Member::Member ( const string&  name )
		: name( name )
		{}

		// ****************** DESTRUCTOR ******************** //

		Member::~Member ( void )
		{}

		// ******************** GETTERS ********************* //

		string
		Member::Name ( void ) const
		{
			return this->name;
		}

		string
		Member::Description ( void ) const
		{
			stringstream  output;
			output << "Member : [name= " << this->Name() << "]";
			return output.str();
		}

		// ******************* OPERATORS ******************** //

		Member&
		Member::operator= ( Member&  member )
		{
			this->name = member.name;
			return (*this);
		}

		// ############### END CLASS MEMBER ################# //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_MEMBER_CPP //
// ############## END FILE MEMBER.CPP ############### //