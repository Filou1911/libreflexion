// ############# BEGIN FILE POINTERDATA.HXX ########## //
#ifndef LIBREFLEXION_MEMBERS_POINTERDATA_HXX
#define LIBREFLEXION_MEMBERS_POINTERDATA_HXX

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //

	namespace Members {

		// ############## BEGIN CLASS POINTERDATA ########### //
			
		// ****************** CONSTRUCTOR ******************* //

		template< typename Type >
		PointerData< Type >::PointerData ( Type*  address )
		: address( address )
		{}

		template< typename Type >
		PointerData< Type >::PointerData ( PointerData& data )
		: address( data.address )
		{}

		// ****************** DESTRUCTOR ******************** //

		template< typename Type >
		PointerData< Type >::~PointerData ( void )
		{}

		// ******************** GETTERS ********************* //

		template< typename Type >
		void*
		PointerData< Type >::Address ( void )
		{
			return reinterpret_cast< void* >( this->address );
		}

		template< typename Type >
		PointerDataI*
		PointerData< Type >::Clone ( void )
		{
			return new PointerData< Type >( *this );
		}

		template< typename Type >
		string
		PointerData< Type >::StrAddress ( void ) const
		{
			stringstream  output;
			output << "0x" << (long)this->address;
			return output.str();
		}

		template< typename Type >
		string
		PointerData< Type >::StrValue ( void ) const
		{
			stringstream  output;
			output << *this->address;
			return output.str();
		}

		// ############### END CLASS POINTERDATA ############ //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_POINTERDATA_HXX //
// ############# END FILE POINTERDATA.HXX ########### //