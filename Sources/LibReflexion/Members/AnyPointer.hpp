// ############ BEGIN FILE ANYPOINTER.HPP ########### //
#ifndef LIBREFLEXION_MEMBERS_ANYPOINTER_HPP
#define LIBREFLEXION_MEMBERS_ANYPOINTER_HPP

// ****************** DEPENDANCES ******************* //

// LOCALS DEPENDACES
#include "Member.hpp"
#include "PointerDataI.hpp"
#include "PointerData.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //

	namespace Members {

		// ############## BEGIN CLASS ANYPOINTER ############ //

		class AnyPointer : public Member {

			// ******************** MEMBERS ********************* //
		private:
			PointerDataI*  data;

			// ****************** CONSTRUCTOR ******************* //
		public:
			AnyPointer ( const string&  name = "Untilted" );

			template< typename Type >
			AnyPointer ( const string&  name, Type* pointer );

			AnyPointer ( AnyPointer&  any );

			// ****************** DESTRUCTOR ******************** //
		public:
			virtual ~AnyPointer ( void );

			// ******************** GETTERS ********************* //
		private:
			const PointerDataI* Data ( void ) const;
			PointerDataI*&		Data ( void );

		public:
			using Member::Name;
			virtual string Description	( void ) const;
			virtual string StrAddress	( void ) const;
			virtual string StrValue		( void ) const;

			template< typename Type >
			Type& Value   ( void );

			template< typename Type >
			Type* Address ( void );

			// ******************* OPERATORS ******************** //
		public:
			AnyPointer&  operator= ( AnyPointer&  any );

			template< typename Type >
			AnyPointer&  operator= ( Type&  value );

		};
		// ############## END CLASS ANYPOINTER ############## //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

// ************* TEMPLATE IMPLEMATATION ************* //

#include "AnyPointer.hxx"

#endif // LIBREFLEXION_MEMBERS_ANYPOINTER_HPP //
// ############ END FILE ANYPOINTER.HPP ############# //
