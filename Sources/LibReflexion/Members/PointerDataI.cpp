// ########### BEGIN FILE POINTERDATAI.CPP ########## //
#ifndef LIBREFLEXION_MEMBERS_POINTERDATAI_CPP
#define LIBREFLEXION_MEMBERS_POINTERDATAI_CPP

// ****************** DEPENDANCES ******************* //

#include "PointerDataI.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //

	namespace Members {

		// ############### BEGIN CLASS POINTERI ############# //


		// ****************** DESTRUCTOR ******************** //

		PointerDataI::~PointerDataI ( void ) {}

		// ############### END CLASS POINTERI ############### //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_POINTERDATAI_HPP //
// ############ END FILE POINTERDATAI.HPP ########### //