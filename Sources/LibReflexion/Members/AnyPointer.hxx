// ############ BEGIN FILE ANYPOINTER.HXX ########### //
#ifndef LIBREFLEXION_MEMBERS_ANYPOINTER_HXX
#define LIBREFLEXION_MEMBERS_ANYPOINTER_HXX

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //

	namespace Members {

		// ############## BEGIN CLASS ANYPOINTER ############ //

		// ****************** CONSTRUCTOR ******************* //

		template< typename Type >
		AnyPointer::AnyPointer ( const string&  name, Type* address )
		: Member( name ), data( new PointerData< Type >( address ) )
		{}

		// ******************** GETTERS ********************* //

		template< typename Type >
		Type&
		AnyPointer::Value ( void )
		{
			return (* this->Address< Type >() );
		}

		template< typename Type >
		Type*
		AnyPointer::Address ( void )
		{
			return reinterpret_cast< Type* >( this->data->Address() );
		}

		// ******************* OPERATORS ******************** //

		template< typename Type >
		AnyPointer&
		AnyPointer::operator= ( Type&  value )
		{
			if ( this->data == NULL )
				cout << "ERROR : This 'AnyPointer' object as no Data associated to it" << endl;

			this->Value< Type >() = value;

			return (*this);
		}

		// ############## END CLASS ANYPOINTER ############## //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_ANYPOINTER_HXX //
// ############ END FILE ANYPOINTER.HXX ############# //
