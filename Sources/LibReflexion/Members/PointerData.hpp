// ############# BEGIN FILE POINTERDATA.HPP ######### //
#ifndef LIBREFLEXION_MEMBERS_POINTERDATA_HPP
#define LIBREFLEXION_MEMBERS_POINTERDATA_HPP

// ****************** DEPENDANCES ******************* //

// LOCALS DEPENDACES
#include "PointerDataI.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //

	namespace Members {

		// ############## BEGIN CLASS POINTERDATA ########### //

		template< typename Type >
		class PointerData : public PointerDataI {

			// ******************** MEMBERS ********************* //
		private:
			Type*  address;

			// ****************** CONSTRUCTOR ******************* //
		public:
			PointerData ( Type*         address );
			PointerData ( PointerData&  data    );

			// ****************** DESTRUCTOR ******************** //

			virtual ~PointerData ( void );

			// ******************** GETTERS ********************* //

			virtual void*			Address 	( void );
			virtual PointerDataI*	Clone		( void );
			virtual string			StrAddress	( void ) const;
			virtual string			StrValue	( void ) const;

		};

		// ############## END CLASS POINTERDATA ############# //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

// ***************** IMPLEMENTATION ***************** //

#include "PointerData.hxx"

#endif // LIBREFLEXION_MEMBERS_POINTERDATA_HPP //
// ############# END FILE POINTERDATA.HPP ########### //