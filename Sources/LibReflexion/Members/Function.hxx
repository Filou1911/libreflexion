// ############ BEGIN FILE FUNCTION.HXX ############# //
#ifndef LIBREFLEXION_MEMBERS_FUNCTION_HXX
#define LIBREFLEXION_MEMBERS_FUNCTION_HXX

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {
	
	// ************* BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ############# BEGIN CLASS FUNCTION ############### //

		// ****************** CONSTRUCTOR ******************* //

		template< typename ReturnType, typename... ArgsTypes >
		Function::Function ( const string&  name, ReturnType(*functionPtr)( ArgsTypes... ) )
		:AnyPointer( name, functionPtr )
		{}

		// ******************** GETTERS ********************* //

		template< typename ReturnType, typename... ArgsTypes >
		ReturnType
		Function::Call ( ArgsTypes... arguments )
		{

			ReturnType(*functionPtr)( ArgsTypes... ) = reinterpret_cast< ReturnType(*)( ArgsTypes... ) >( this->Address< ReturnType(*)( ArgsTypes... ) >() );
			return functionPtr( arguments... );
		}

		// ******************* OPERATORS ******************** //

		template< typename ReturnType, typename... ArgsTypes >
		Function&
		Function::operator= ( ReturnType(*functionPtr)( ArgsTypes... ) )
		{
			cout << "ERROR : Function::operator=( ptrFunction ) is uninplemented" << endl;
			//this->AnyPointer::operator=( ptrFunction );
			return (*this);
		}

		template< typename ReturnType, typename... ArgsTypes >
		ReturnType
		Function::operator() ( ArgsTypes... arguments )
		{
			return this->Call< ReturnType, ArgsTypes... >( arguments... );
		}

		// ############### END CLASS FUNCTION ############### //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_FUNCTION_HXX //
// ############# END FILE FUNCTION.HXX ############## //