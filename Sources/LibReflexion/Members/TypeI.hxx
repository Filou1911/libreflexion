// ############## BEGIN FILE TYPEI.HXX ############# //
#ifndef LIBREFLEXION_MEMBERS_TYPEI_HXX
#define LIBREFLEXION_MEMBERS_TYPEI_HXX

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ############### BEGIN CLASS TYPEI ############### //

		// ****************** CONSTRUCTOR ******************* //
		
		template< typename TypeName >
		TypeI< TypeName >::TypeI ( const string& name, UInt size )
		: Type( name, size )
		{}

		// ****************** DESTRUCTOR ******************** //

		template< typename TypeName >
		TypeI< TypeName >::~TypeI ( void )
		{}

		// ******************* UTILITIES ******************** //
		/*
		template< typename TypeName >
		GAny
		GTypeI< TypeName >::Allocate ( void ) const
		{
			TypeName newborn;
			return GAny( newborn );
		}
		*/
		// ################ END CLASS TYPEI ################# //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_TYPEI_HXX //
// ############### END FILE TYPEI.HXX ############### //