// ############# BEGIN FILE VARIABLE.HPP ############ //
#ifndef LIBREFLEXION_MEMBERS_VARIABLE_HPP
#define LIBREFLEXION_MEMBERS_VARIABLE_HPP

// ****************** DEPENDANCES ******************* //

// LOCALS DEPENDANCES : //
#include "AnyPointer.hpp"

// ############# API MACROS DEFINITIONS ############# //

#define R_VARIABLE( VARIABLE ) new LibReflexion::Members::Variable( #VARIABLE, &( VARIABLE ) )

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ################ BEGIN CLASS GVARIABLE ############### //

		class Variable : public AnyPointer {

			// ****************** CONSTRUCTOR ******************* //
		public:
			Variable ( const string&  name = "Untilted" );

			template< typename Type >
			Variable ( const string&  name, Type* ptrData );

			Variable ( Variable&  variable );

			// ****************** DESTRUCTOR ******************** //

			virtual ~Variable ( void );

			// ******************** GETTERS ********************* //
		public:
			using AnyPointer::Name;
			using AnyPointer::StrAddress;
			using AnyPointer::StrValue;
			
			virtual string Description ( void ) const;

			template< typename Type >
			Type&  Value ( void );

			// ******************* OPERATORS ******************** //

			Variable&  operator= ( Variable&  variable );

			template< typename Type >
			Variable&  operator= ( Type  value );

			template< typename Type >
			operator Type ();
		};
		// ############### END CLASS GVARIABLE ################## //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

// ************* TEMPLATE IMPLEMATATION ************* //

#include "Variable.hxx"

#endif // LIBREFLEXION_MEMBERS_VARIABLE_HPP //
// ############## END FILE VARIABLE.HPP ############# //