// ############## BEGIN FILE SCOPE.HPP ############## //
#ifndef LIBREFLEXION_MEMBERS_SCOPE_HPP
#define LIBREFLEXION_MEMBERS_SCOPE_HPP

// ****************** DEPENDANCES ******************* //

// INTERNALS DEPENDANCES
#include "Containers/Dictionary.hpp"

// LOCALS DEPENDACES
#include "Member.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ######## FORWARD DEFINITION OF CLASS NODE ######## //

		#define Scope NameSpace
		
		// ############### BEGIN CLASS GSCOPE ############### //

		class Scope : public Member, private Containers::Dictionary< Member > {

			// ******************** MEMBERS ********************* //
		private:
			Scope* ptrParent;

			// ****************** CONSTRUCTOR ******************* //
		public:
			 Scope ( Scope* ptrParent = NULL, const string&  name = "Untilted" );

			 template < typename... ArgsTypes >
			 Scope ( Scope* ptrParent, const string&  name, ArgsTypes... members );

			 Scope ( Scope&  scope );

			// ****************** DESTRUCTOR ******************** //

			virtual ~Scope ( void );

			// ******************** GETTERS ********************* //
		public:
			virtual Scope*&				Parent		( void );
			virtual Member* 			GetMember	( const string&  name );
			virtual vector< string >	ListMembers	( bool expandScopes = false );
			virtual string				Description	( void ) const;

			// ******************** SETTERS ********************* //
		public:
			// REMOVE MEMBERS : //
			using Containers::Dictionary< Member >::RemoveAll;

			// ADD MEMBERS : //
			virtual Member*	AddMember ( Member* ptrMember );

			bool AddMembers ( void );
			template < typename... ArgsTypes >
			bool AddMembers ( Member*  ptrMember, ArgsTypes... rest );

			// ******************* OPERATORS ******************** //

			virtual Scope& operator+= ( Scope&  scope );
			
		};
		// ################ END CLASS SCOPE ################# //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

// ************* TEMPLATE IMPLEMATATION ************* //

#include "Scope.hxx"

#endif // LIBREFLEXION_MEMBERS_SCOPE_HPP //
// ############### END FILE SCOPE.HPP ############### //