// ############ BEGIN FILE ANYPOINTER.CPP ########### //
#ifndef LIBREFLEXION_MEMBERS_ANYPOINTER_CPP
#define LIBREFLEXION_MEMBERS_ANYPOINTER_CPP

// ****************** DEPENDANCES ******************* //

// LOCALS DEPENDACES
#include "AnyPointer.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //

	namespace Members {

		// ############## BEGIN CLASS ANYPOINTER ############ //

		// ****************** CONSTRUCTOR ******************* //

		AnyPointer::AnyPointer ( const string&  name )
		: Member( name ), data( NULL )
		{}

		AnyPointer::AnyPointer ( AnyPointer&  any )
		: Member( any ), data( any.Data()->Clone() )
		{}

		// ****************** DESTRUCTOR ******************** //

		AnyPointer::~AnyPointer ( void )
		{
			delete this->data;
		}

		// ******************** GETTERS ********************* //

		const PointerDataI*
		AnyPointer::Data ( void ) const
		{
			return this->data;
		}

		PointerDataI*&
		AnyPointer::Data ( void )
		{
			return this->data;
		}

		string
		AnyPointer::Description ( void ) const
		{
			stringstream  output;
			output << "AnyPointer : [name= " << this->Name() << ", data= " << this->StrAddress() << "]";
			return output.str();
		}

		string
		AnyPointer::StrAddress ( void ) const
		{
			return this->Data()->StrAddress();
		}

		string
		AnyPointer::StrValue ( void ) const
		{
			return this->Data()->StrValue();
		}

		// ******************* OPERATORS ******************** //
		
		AnyPointer&
		AnyPointer::operator= ( AnyPointer&  any )
		{
			this->Member::operator=( any );

			if ( this->Data() != NULL )
				delete this->Data();
			
			this->Data() = any.Data()->Clone();

			return (*this);
		}

		// ############### END CLASS ANYPOINTER ############# //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_ANYPOINTER_CPP //
// ############### END FILE ANYPOINTER.CPP ################# //
