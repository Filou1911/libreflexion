// ############## BEGIN FILE TYPE.CPP ############### //
#ifndef LIBREFLEXION_MEMBERS_TYPE_CPP
#define LIBREFLEXION_MEMBERS_TYPE_CPP

// ****************** DEPENDANCES ******************* //

// LOCAL DEPENDANCES : //
#include "Type.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ################ BEGIN CLASS TYPE ################ //

		// ****************** CONSTRUCTOR ******************* //
		
		Type::Type ( const string& name, UInt size )
		: Member( name ), size( size )
		{}

		// ****************** DESTRUCTOR ******************** //

		Type::~Type ( void ) {}

		// ******************** GETTERS ********************* //

		UInt
		Type::Size ( void ) const
		{
			return this->size;
		}

		// ################ END CLASS TYPE ################# //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // GREFLEXION_GTYPES_GTYPEI_CPP //
// ############## END FILE GTYPEI.CPP ############### //