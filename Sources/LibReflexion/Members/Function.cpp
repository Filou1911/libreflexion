// ############ BEGIN FILE FUNCTION.CPP ############# //
#ifndef LIBREFLEXION_MEMBERS_FUNCTION_CPP
#define LIBREFLEXION_MEMBERS_FUNCTION_CPP

// ****************** DEPENDANCES ******************* //

#include "Function.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //

	namespace Members {

		// ############## BEGIN CLASS FUNCTION ############## //

		// ****************** CONSTRUCTOR ******************* //

		Function::Function ( const string&  name )
		: AnyPointer( name )
		{}

		// ****************** DESTRUCTOR ******************** //

		Function::~Function ( void )
		{}

		// ******************** GETTERS ********************* //

		string
		Function::Description ( void ) const
		{
			stringstream  output;
			output << "Function : [name= " << this->Name() << ", address= " << this->StrAddress() << "]";
			return output.str();
		}

		// ******************* OPERATORS ******************** //

		Function&
		Function::operator= ( Function&  function )
		{
			this->AnyPointer::operator=( static_cast< AnyPointer& >( function ) );
			return (*this);
		}

		// ############### END CLASS FUNCTION ############### //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_FUNCTION_CPP //
// ############# END FILE FUNCTION.CPP ############## //
