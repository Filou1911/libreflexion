// ############ BEGIN FILE NAMESPACE.HPP ############ //
#ifndef LIBREFLEXION_MEMBERS_NAMESPACE_HPP
#define LIBREFLEXION_MEMBERS_NAMESPACE_HPP

// ****************** DEPENDANCES ******************* //

// LOCAL DEPENDANCES
#include "Scope.hpp"

// ############# API MACROS DEFINITIONS ############# //

#define R_NAMESPACE( NAME, ... ) new LibReflexion::Members::NameSpace( LibReflexion::CurrentScope(), NAME, __VA_ARGS__ )

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ############## BEGIN CLASS NAMESPACE ############## //
		
		using NameSpace = Scope;

		// ############### END CLASS NAMESPACE ############### //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_NAMESPACE_HXX //
// ############# END FILE NAMESPACE.HXX ############# //