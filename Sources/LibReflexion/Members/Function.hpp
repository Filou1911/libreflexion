// ############ BEGIN FILE FUNCTION.HPP ############# //
#ifndef LIBREFLEXION_MEMBERS_FUNCTION_HPP
#define LIBREFLEXION_MEMBERS_FUNCTION_HPP

// ****************** DEPENDANCES ******************* //

// LOCALS DEPENDACES
#include "AnyPointer.hpp"

// ############# API MACROS DEFINITIONS ############# //

#define R_FUNCTION( FUNCTION ) new LibReflexion::Members::Function( #FUNCTION, FUNCTION )

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //

	namespace Members {

		// ############## BEGIN CLASS FUNCTION ############## //

		class Function : public AnyPointer {

			// ****************** CONSTRUCTOR ******************* //
		public:
			Function ( const string&  name = "Untilted" );

			template< typename ReturnType, typename... ArgsTypes >
			Function ( const string&  name, ReturnType(*functionPtr)( ArgsTypes... ) );

			// ****************** DESTRUCTOR ******************** //

			virtual ~Function ( void );

			// ******************** GETTERS ********************* //
		public:
			using AnyPointer::Name;
			using AnyPointer::StrAddress;
			using AnyPointer::StrValue;

			virtual string Description ( void ) const;

			template< typename ReturnType, typename... ArgsTypes >
			ReturnType Call ( ArgsTypes... arguments );

			// ******************* OPERATORS ******************** //

			Function&  operator= ( Function&  function );

			template< typename ReturnType, typename... ArgsTypes >
			Function&  operator= ( ReturnType(*functionPtr)( ArgsTypes... ) );

			template< typename ReturnType, typename... ArgsTypes >
			ReturnType operator() ( ArgsTypes... arguments );

		};
		// ############### END CLASS FUNCTION ############### //

	}
	// ************** END NAMESPACE MEMBERS ************* //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

// ************* TEMPLATE IMPLEMATATION ************* //

#include "Function.hxx"

#endif // LIBREFLEXION_MEMBERS_FUNCTION_HPP //
// ############# END FILE FUNCTION.HPP ############## //
