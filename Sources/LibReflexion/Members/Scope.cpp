// ############## BEGIN FILE SCOPE.CPP ############## //
#ifndef LIBREFLEXION_MEMBERS_SCOPE_CPP
#define LIBREFLEXION_MEMBERS_SCOPE_CPP

// ****************** DEPENDANCES ******************* //

#include "Scope.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE GTYPES ************* //
		
	namespace Members {

		// ############### BEGIN CLASS SCOPE ################ //

		// ****************** CONSTRUCTOR ******************* //

		Scope::Scope ( Scope* ptrParent, const string&  name )
		: Member( name ), ptrParent( ptrParent )
		{}

		Scope::Scope ( Scope&  scope )
		: Member( scope ), Containers::Dictionary< Member >( scope ), ptrParent( scope.Parent() )
		{}

		// ****************** DESTRUCTOR ******************** //

		Scope::~Scope ( void )
		{}

		// ******************** GETTERS ********************* //

		Scope*&
		Scope::Parent ( void )
		{
			return this->ptrParent;
		}

		Member*
		Scope::GetMember ( const string&  name )
		{
			// Check if the name requires a scope resolution first (recursive calls):
			int position( name.find( "::" ) );

			if ( position != string::npos )
			{
				string memberName( name.begin(), name.begin()+position );
				string rest( name.begin()+position+2, name.end() );
 
 				// Try to find the find member
 				Member* ptrMember( this->GetEntry( memberName ) );

 				// If not found, not found
				if ( ptrMember == NULL )
					return NULL;

				// if it is found, and it is a scope, search the rest recursivly:
				return dynamic_cast< Scope* >( ptrMember )->GetMember( rest );
			}

			// Chech if the name appearse in 'this' scope:
			Member* ptrMember( this->GetEntry( name ) );

			if ( ptrMember != NULL )
				return ptrMember;

			// if 'this' scope has a parent, look in it's scope (this is recursif on all direct parent):
			if ( this->Parent() != NULL )
				return this->Parent()->GetMember( name );

			// then it doesn't exist:
			return NULL;
		}

		vector< string >
		Scope::ListMembers ( bool expandScopes )
		{
			if ( expandScopes == false )
				return this->ListEntries();
			
			vector< string > lstFinal;
			for ( const string&  memberName : this->ListEntries() )
			{
				// Add a tab before the name
				string member;
				member += memberName;

				// Copy to the final list
				lstFinal.push_back( member );

				// If the member is a scope, a recursive call is needed:
				Scope* ptrScope( dynamic_cast< Scope* >( this->GetMember( memberName ) ) );

				if ( ptrScope != NULL )
				{	
					for ( string&  subMemberName : ptrScope->ListMembers( expandScopes ) )
						lstFinal.push_back( "\t" + subMemberName );
				}
			}
			return lstFinal;
		}

		string
		Scope::Description ( void ) const
		{
			stringstream  output;
			output << "Scope : [name= " << this->Name() << "]";
			return output.str();
		}

		// ******************** SETTERS ********************* //

		Member*
		Scope::AddMember ( Member* ptrMember )
		{
			// If the new member and the existing are scopes, merge them:
			Scope*      ptrNew( dynamic_cast< Scope* >( ptrMember ) );
			Scope* ptrExisting( dynamic_cast< Scope* >( this->GetMember( ptrMember->Name() ) ) );

			if ( ptrNew != NULL && ptrExisting != NULL )
			{
				// Copy the content of the new member to the existing one
				ptrExisting->operator+=( *ptrNew );


				// Empty scope without deleting it's content, and delete it
				ptrNew->RemoveAll( false );
				delete ptrNew;

				// return the existing
				ptrMember = ptrExisting;
				return ptrExisting;
			}

			// Otherwise simply add the new member and return it (will override any other existing member)
			this->AddEntry( ptrMember, ptrMember->Name() );
			return ptrMember;
		}

		bool
		Scope::AddMembers ( void )
		{
			return true;
		}

		// ******************* OPERATORS ******************** //

		Scope&
		Scope::operator+= ( Scope&  scope )
		{
			// add every members of the argument scope to the current one
			for ( const string&  memberName : scope.ListMembers() )
				this->AddMember( scope.GetMember( memberName ) );

			return (*this);
		}

		// ################ END CLASS GSCOPE ################ //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_SCOPE_CPP //
// ############### END FILE SCOPE.CPP ############### //