// ############## BEGIN FILE TYPEI.HPP ############# //
#ifndef LIBREFLEXION_MEMBERS_TYPEI_HPP
#define LIBREFLEXION_MEMBERS_TYPEI_HPP

// ****************** DEPENDANCES ******************* //

// LOCAL DEPENDANCES : //
#include "Type.hpp"

// ############# API MACROS DEFINITIONS ############# //

#define R_TYPE( TYPE ) new LibReflexion::Members::TypeI< TYPE >( #TYPE, sizeof( TYPE ) )

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************ BEGIN NAMESPACE MEMBERS ************* //
		
	namespace Members {

		// ################ BEGIN CLASS GTYPEI ############## //

		template< typename TypeName >
		class TypeI : public Type {

			// ****************** CONSTRUCTOR ******************* //
		public:
			TypeI ( const string& name, UInt size );

			// ****************** DESTRUCTOR ******************** //

			virtual ~TypeI ( void );

			// ******************** GETTERS ********************* //

			using Type::Name;
			using Type::Size;

			// ******************* UTILITIES ******************** //

			// virtual GAny Allocate ( void ) const;

		};
		// ################ END CLASS TYPEI ################# //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

// ************* TEMPLATE IMPLEMATATION ************* //

#include "TypeI.hxx"

#endif // LIBREFLEXION_MEMBERS_TYPEI_HPP //
// ############### END FILE TYPEI.HPP ############### //