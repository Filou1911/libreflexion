// ############# BEGIN FILE MEMBER.HPP ############## //
#ifndef LIBREFLEXION_MEMBERS_MEMBER_HPP
#define LIBREFLEXION_MEMBERS_MEMBER_HPP

// ****************** DEPENDANCES ******************* //

#include "LibDependances.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// ************* BEGIN NAMESPACE MEMBERS ************ //
		
	namespace Members {

		// ############## BEGIN CLASS GMEMBER ############### //

		class Member {

			// ******************** MEMBERS ********************* //
		private:
			string name;

			// ****************** CONSTRUCTOR ******************* //
		public:
			 Member ( const string&  name = "Untilted" );

			// ****************** DESTRUCTOR ******************** //

			virtual ~Member ( void );

			// ******************** GETTERS ********************* //

			virtual	string Name			( void ) const final;
			virtual string Description	( void ) const;

			// ******************* OPERATORS ******************** //

			Member&  operator= ( Member&  member );

		};
		// ############### END CLASS MEMBER ################# //

	}
	// ************* END NAMESPACE MEMBERS ************** //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_MEMBERS_MEMBER_HPP //
// ############## END FILE MEMBER.HPP ############### //