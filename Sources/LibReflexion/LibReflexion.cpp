// ########## BEGIN FILE LIBREFLEXION.CPP ########### //
#ifndef LIBREFLEXION_CPP
#define LIBREFLEXION_CPP

// ****************** DEPENDANCES ******************* //

#include "LibReflexion.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// SCOPE ACCESS : //

	Scope*
	GlobalScope ( void )
	{
		// Declaration of the Global scope, and initialisation on first call:
		static Scope  globalScope( NULL, "Global Scope" );

		// Return it's adress:
		return &( globalScope );
	}

	Scope*&
	CurrentScope ( void )
	{
		// Declaratio of the current scope, and initialisation on first call
		// with the adress of the global scope:
		static Scope*  currentScope( GlobalScope() );

		// Return the current cope adress:
		return currentScope;
	}

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_HPP //
// ########### END FILE LIBREFLEXION.CPP ############ //