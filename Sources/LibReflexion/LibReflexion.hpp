// ########## BEGIN FILE LIBREFLEXION.HPP ########### //
#ifndef LIBREFLEXION_HPP
#define LIBREFLEXION_HPP

// ****************** DEPENDANCES ******************* //

// INTERNALS DEPENDANCES
#include "LibDependances.hpp"
#include "Members/Members.hpp"

// ############# API MACROS DEFINITIONS ############# //

#define R_CONCATENATE_1( A, B ) A ## B
#define R_CONCATENATE( A, B ) R_CONCATENATE_1( A, B )

#define R_METADATA( ... ) \
static bool R_CONCATENATE( metadata_, __COUNTER__ ) = LibReflexion::CurrentScope()->AddMembers( __VA_ARGS__ )

#define R_TEMPLATENAME( NAME, TYPE ) std::string( #NAME ) + std::string( "< " ) + std::string( #TYPE ) + std::string( " >" )

// ***************** DEFINE MEMBERS ***************** //

#define R_TMPFUN( TYPE, NAME, ADRESS, ... ) new LibReflexion::Members::Function< TYPE, __VA_ARGS__ >( NAME, &( ADRESS ) )

// ***************** ACCESS MEMBERS ***************** //

// VARIABLES : //

#define R_VAR( NAME ) (*dynamic_cast< LibReflexion::Members::Variable* >( LibReflexion::CurrentScope()->GetMember( NAME ) ))

// FUNCTIONS : //

#define R_FUN( NAME ) (*dynamic_cast< LibReflexion::Members::Function* >( LibReflexion::CurrentScope()->GetMember( NAME ) ))

// TYPES : //

#define R_GET_TYPE( NAME ) dynamic_cast< Reflexion::Members::Type* >( LibReflexion::CurrentScope()->GetMember( NAME ) )

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	using namespace Members;

	// SCOPE ACCESS

	Scope*   GlobalScope  ( void );
	Scope*&  CurrentScope ( void );

}
// ************ END NAMESPACE GREFLEXION ************ //

#endif // LIBREFLEXION_HPP //
// ########### END FILE LIBREFLEXION.HPP ############ //