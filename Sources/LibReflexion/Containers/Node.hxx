// ############## BEGIN FILE NODE.HXX ############### //
#ifndef LIBREFLEXION_CONTAINERS_NODE_HXX
#define LIBREFLEXION_CONTAINERS_NODE_HXX

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// *********** BEGIN NAMESPACE CONTAINERS *********** //

		namespace Containers {

		// ################ BEGIN CLASS NODE ################ //

		// ****************** CONSTRUCTOR ******************* //

		template< typename Type >
		Node< Type >::Node ( Type*  entry, const string&  name )
		: entry( NULL )
		{
			this->AddEntry( entry, name );
		}

		template< typename Type >
		Node< Type >::Node ( Node< Type >&  node )
		: links( node.Links() ), entry( node.Entry() )
		{}

		// ****************** DESTRUCTOR ******************** //

		template< typename Type >
		Node< Type >::~Node ( void )
		{
			this->RemoveAll( true );
		}

		// ******************** GETTERS ********************* //
		
		template< typename Type >
		const vector< Link< Type >* >&
		Node< Type >::Links ( void ) const
		{
			return this->links;
		}

		template< typename Type >
		vector< Link< Type >* >&
		Node< Type >::Links ( void )
		{
			return this->links;
		}

		template< typename Type >
		const Type*
		Node< Type >::Entry ( void ) const
		{
			return this->entry;
		}

		template< typename Type >
		Type*&
		Node< Type >::Entry ( void )
		{
			return this->entry;
		}

		template< typename Type >
		bool
		Node< Type >::IsEmpty ( void ) const
		{
			// If the node has no entry and no child, he is empty.
			if ( this->Links().empty() && this->Entry() == NULL )
				return true;

			// If it has child, all child must be empty.
			for ( const Link< Type >* ptrLink : this->Links() )
			{
				if ( ! ptrLink->Child()->IsEmpty() )
					return false;
			}

			// If all child are empty, it is empty.
			return true;
		}
		
		template< typename Type >
		vector< string >
		Node< Type >::ListEntries ( void ) const
		{
			vector< string > entries;

			if ( this->Entry() != NULL )
				entries.push_back( "\0" );

			for ( const Link< Type >* ptrLink : this->Links() )
				for ( const string&  subEntry : ptrLink->ListEntries() )
					entries.push_back( subEntry );

			return entries;
		}

		template< typename Type >
		Type*
		Node< Type >::GetEntry ( const string&  name )
		{
			if ( name.front() == '\0' )
				return this->Entry();

			for ( Link< Type >* ptrLink : this->Links() )
			{
				if ( ptrLink->Letter() == name.front() )
					return ptrLink->Child()->GetEntry( string( name, 1 ) );
			}
			return NULL;
		}

		// ******************** SETTERS ********************* //

		template< typename Type >
		Type*
		Node< Type >::AddEntry ( Type*  entry, const string&  name )
		{
			// if the name is '\0', we change the entry in this node
			if ( name.front() == '\0' )
			{
				// delete previous
				if ( this->Entry() != NULL )
					delete this->Entry();

				// set the new value and return
				this->Entry() = entry;
				return entry;
			}

			// if not, check the existing children and find the one with the good letter
			for ( Link< Type >* ptrLink : this->Links() )
			{
				if ( ptrLink->Letter() == name.front() )
					return ptrLink->Child()->AddEntry( entry, string( name, 1 ) );
			}

			// if it doesnt exist, create it
			this->Links().push_back( new Link< Type >( name.front(), entry, string( name, 1 ) ) );

			return entry;
		}

		template< typename Type >
		Type*
		Node< Type >::RemoveEntry ( const string&  name )
		{
			// Local result:
			Type*  ptrEntry( NULL );

			// if the name is '\0', we get the entry in this node
			if ( name.front() == '\0' )
			{
				// Get the entry:
				ptrEntry = this->Entry();

				// Set the node entry to NULL
				this->Entry() = NULL;
			}

			// if not, check the existing children and find the one with the good letter
			for ( Link< Type >* ptrLink : this->Links() )
			{
				if ( ptrLink->Letter() == name.front() )
					return ptrLink->Child()->RemoveEntry( string( name, 1 ) );
			}

			// Delete empty branches of the dictionary:
			this->RemoveEmpty();

			// if not found
			return NULL;
		}

		template< typename Type >
		Node< Type >&
		Node< Type >::RemoveEmpty ( void )
		{
			for ( UInt index( 0 ) ; index < this->Links().size() ; ++index )
			{
				Link< Type >* ptrLink( this->Links()[ index ] );

				if ( ptrLink->Child()->IsEmpty() )
				{
					this->Links().erase( this->Links().begin()+index );
					delete ptrLink;
				}
			}
			return (*this);
		}

		template< typename Type >
		Node< Type >&
		Node< Type >::RemoveAll ( bool deleteEntries )
		{
			// delete the entry
			if ( this->Entry() != NULL )
			{
				if ( deleteEntries )
					delete this->Entry();

				this->Entry() = NULL;
			}

			// delete the links
			while ( ! this->links.empty() )
			{
				this->links.back()->Child()->RemoveAll( deleteEntries );

				delete this->links.back();
				
				this->links.pop_back();
			}

			return (*this);
		}

		// ******************* OPERATORS ******************** //

		template< typename Type >
		Node< Type >&
		Node< Type >::operator= ( Node< Type >&  node )
		{
			// Clean the node
			this->RemoveAll( true );

			// Copy
			this->Entry() = node.Entry();
			this->Links() = node.Links();

			return (*this);
		}

		template< typename Type >
		Node< Type >&
		Node< Type >::operator+= ( Node< Type >&  node )
		{
			// Assign the entry if there is one
			if ( node.Entry() != NULL )
			{
				// Check if one already exists
				if ( this->Entry() != NULL )
					delete this->Entry();

				// Assign it
				this->Entry() = node.Entry();
			}

			// For every new link (from node)
			for ( auto newLink : node.Links() )
			{
				bool isAssigned( false );

				// Check if a link with the same letter exist
				for ( auto oldLink : this->Links() )
				{
					// If yes, do recursive call on childrens
					if ( newLink->Letter() == oldLink->Letter() )
					{
						oldLink->Child()->operator+=( *newLink->Child() );
						isAssigned = true;
						break;
					}
				}

				// if not, add the link
				if ( ! isAssigned )
					this->Links().push_back( newLink );
			}

			return (*this);
		}

		// ################# END CLASS NODE ################# //

	}
	// ************ END NAMESPACE CONTAINERS ************ //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_CONTAINERS_NODE_HXX //
// ################ END FILE NODE.HXX ############### //