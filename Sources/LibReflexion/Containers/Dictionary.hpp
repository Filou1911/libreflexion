// ########### BEGIN FILE DICTIONARY.HPP ############ //
#ifndef LIBREFLEXION_CONTAINERS_DICTIONARY_HPP
#define LIBREFLEXION_CONTAINERS_DICTIONARY_HPP

// ****************** DEPENDANCES ******************* //

// INTERNAL DEPENDANCES
#include "LibDependances.hpp"

// LOCALS DEPENDACES
#include "Node.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// *********** BEGIN NAMESPACE CONTAINERS *********** //

	namespace Containers {

		// ############# BEGIN CLASS DICTIONARY ############# //

		template< typename Type >
		using Dictionary = Node< Type >;

		// ############## END CLASS DICTIONARY ############## //

	}
	// ************ END NAMESPACE CONTAINERS ************ //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

#endif // LIBREFLEXION_CONTAINERS_DICTIONARY_HPP
// ############ END FILE DICTIONARY.HPP ############# //