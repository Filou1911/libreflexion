// ############## BEGIN FILE LINK.HPP ############### //
#ifndef LIBREFLEXION_CONTAINERS_LINK_HPP
#define LIBREFLEXION_CONTAINERS_LINK_HPP

// ****************** DEPENDANCES ******************* //

// INTERNAL DEPENDANCES : //
#include "LibDependances.hpp"

// LOCALS DEPENDACES : //
#include "Node.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ****x****** //

namespace LibReflexion {

	// *********** BEGIN NAMESPACE CONTAINERS *********** //

	namespace Containers {

		// ######## FORWARD DEFINITION OF CLASS NODE ######## //

		template< typename Type >
		class Node;

		// ################ BEGIN CLASS LINK ################ //

		template< typename Type >
		class Link {

			// ******************** MEMBERS ********************* //
		private:
			Node< Type >*	child;
			char			letter;

			// ****************** CONSTRUCTOR ******************* //
		public:
			Link ( char  letter = '\0', Type*  member = NULL, const string&  name = "" );

			// ****************** DESTRUCTOR ******************** //
		public:
			virtual ~Link ( void );

			// ******************** GETTERS ********************* //
		public:
			virtual	const	Node< Type >*	Child		( void ) const	final;
			virtual			Node< Type >*	Child		( void )		final;
			virtual			char			Letter		( void ) const	final;
			virtual			char&			Letter		( void ) 		final;
			virtual vector< string >  		ListEntries ( void ) const	final;

		};
		// ################# END CLASS LINK ################# //
	
	}
	// ************ END NAMESPACE CONTAINERS ************ //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

// ************* TEMPLATE IMPLEMATATION ************* //

#include "Link.hxx"

#endif // LIBREFLEXION_CONTAINERS_LINK_HPP
// ################ END FILE LINK.HPP ############### //