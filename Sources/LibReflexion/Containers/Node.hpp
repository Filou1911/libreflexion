// ############### BEGIN FILE NODE.HPP ############## //
#ifndef LIBREFLEXION_CONTAINERS_NODE_HPP
#define LIBREFLEXION_CONTAINERS_NODE_HPP

// ****************** DEPENDANCES ******************* //

// INTERNAL DEPENDANCES
#include "LibDependances.hpp"

// LOCALS DEPENDACES
#include "Link.hpp"

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {
	
	// *********** BEGIN NAMESPACE CONTAINERS *********** //

	namespace Containers {

		// ######## FORWARD DEFINITION OF CLASS LINK ######## //

		template< typename Type >
		class Link;

		// ################ BEGIN CLASS NODE ################ //

		template< typename Type >
		class Node {

			// ******************** MEMBERS ********************* //
		private:
			vector< Link< Type >* >	links;
			Type*					entry;

			// ****************** CONSTRUCTOR ******************* //
		public:
			Node ( Type*  entry = NULL, const string&  name = "" );
			Node ( Node< Type >&  node );

			// ****************** DESTRUCTOR ******************** //

			virtual ~Node ( void );

			// ******************** GETTERS ********************* //
		private:
			virtual const vector< Link< Type >* >&	Links	( void ) const;
			virtual 	  vector< Link< Type >* >&	Links	( void );
			virtual const Type* 					Entry	( void ) const;
			virtual 	  Type*&					Entry	( void );

		public:
			virtual bool				IsEmpty		( void ) const;
			virtual vector< string >	ListEntries	( void ) const;
			virtual Type*			  	GetEntry	( const string&  name );

			// ******************** SETTERS ********************* //
		public:
			virtual Type*			AddEntry	( Type*  entry, const string&  name );
			virtual Type*			RemoveEntry	( const string&  name );
			virtual Node< Type >&	RemoveEmpty ( void );
			virtual Node< Type >&	RemoveAll	( bool deleteEntries = true );

			// ******************* OPERATORS ******************** //

			virtual Node< Type >& operator=  ( Node< Type >&  node );
			virtual Node< Type >& operator+= ( Node< Type >&  node );

		};
		// ################# END CLASS NODE ################# //

	}
	// ************ END NAMESPACE CONTAINERS ************ //

}
// *********** END NAMESPACE LIBREFLEXION *********** //

// ************* TEMPLATE IMPLEMATATION ************* //

#include "Node.hxx"

#endif // LIBREFLEXION_CONTAINERS_NODE_HPP
// ################ END FILE NODE.HPP ############### //