// ############### BEGIN FILE LINK.HXX ############## //
#ifndef LIBREFLEXION_CONTAINERS_LINK_HXX
#define LIBREFLEXION_CONTAINERS_LINK_HXX

// ********** BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// *********** BEGIN NAMESPACE CONTAINERS *********** //

		namespace Containers {

		// ############### BEGIN CLASS GLINK ################ //

		// ****************** CONSTRUCTOR ******************* //

		template< typename Type >
		Link< Type >::Link ( char  letter, Type*  member, const string&  name )
		: child( new Node< Type >( member, name ) ), letter( letter )
		{}

		// ****************** DESTRUCTOR ******************** //

		template< typename Type >
		Link< Type >::~Link ( void )
		{
			if ( this->child != NULL )
				delete this->child;
		}

		// ******************** GETTERS ********************* //

		template< typename Type >
		const Node< Type >*
		Link< Type >::Child ( void ) const
		{
			return this->child;
		}

		template< typename Type >
		Node< Type >*
		Link< Type >::Child ( void )
		{
			return this->child;
		}

		template< typename Type >
		char
		Link< Type >::Letter ( void ) const
		{
			return this->letter;
		}

		template< typename Type >
		char&
		Link< Type >::Letter ( void )
		{
			return this->letter;
		}

		template< typename Type >
		vector< string >  
		Link< Type >::ListEntries ( void ) const
		{
			vector< string > childEntries( this->Child()->ListEntries() );

			for ( string&  subEntry : childEntries )
			{
				string newString;

				newString += this->Letter();
				newString += subEntry;

				subEntry = newString;
			}

			return childEntries;
		}
		
		// ################# END CLASS LINK ################# //

	}
	// ************ END NAMESPACE CONTAINERS ************ //

}
// ************ END NAMESPACE GREFLEXION ************ //

#endif // LIBREFLEXION_CONTAINERS_LINK_HXX
// ################ END FILE LINK.HXX ############### //