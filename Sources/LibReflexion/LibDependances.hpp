// ########### BEGIN FILE LIBDEPENDANCES.HPP ######### //
#ifndef LIBREFLEXION_LIBDEPENDANCES_HPP
#define LIBREFLEXION_LIBDEPENDANCES_HPP

// ****************** DEPENDANCES ******************* //

// STANDARDS DEPENDANCES : //
#include <string>
#include <sstream>
#include <vector>
#include <iostream>

// ********* BEGIN NAMESPACE LIBREFLEXION ********** //

namespace LibReflexion {

	// NAMESAPCES : //
	using namespace std;

	// TYPE DEFINITIONS : //
	using UInt = unsigned int;

}
// *********** END NAMESPACE LIBREFLEXION ********** //

#endif // LIBREFLEXION_LIBDEPENDANCES_HPP //
// ########### END FILE LIBDEPENDANCES.HPP ########## //