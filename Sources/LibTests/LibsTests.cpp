// #################################################################################################### //
// ###                                          DEPENDANCES                                         ### //
// #################################################################################################### //

// STANDARDS DEPENDANCES : //
#include <iostream>
#include <string>

// LIBRARIES DEPENDANCES : //
#include "LibReflexion/LibReflexion.hpp"

// #################################################################################################### //
// ###                          EXEMPLES OF VARIOUS REFLEXIVES DEFINITIONS                          ### //
// #################################################################################################### //

// REFLEXIVES VARIABLES DEFINITIONS : ***************************************************************** //

int globalVariable1 ( 1 );
int globalVariable2 ( 2 );

R_METADATA(
	R_NAMESPACE( "LibTests",
		R_VARIABLE( globalVariable1 ),
		R_VARIABLE( globalVariable2 )
	)
);

// REFLEXIVES FUNCTIONS DEFINITIONS : ***************************************************************** //

// NORMALS

int add       ( int a, int b ) { return a + b; }
int substract ( int a, int b ) { return a - b; }

R_METADATA(
	R_NAMESPACE( "LibTests",
		R_FUNCTION( add       ),
		R_FUNCTION( substract )
	)
);

// REFLEXIVES TYPES DEFINITIONS : ********************************************************************* //

R_METADATA(
	R_NAMESPACE( "LibTests",
		R_TYPE( int ),
		R_TYPE( double )
	)
);

// #################################################################################################### //
// ###                         EXEMPLES OF VARIOUS REFLEXIVES APPLICATIONS                          ### //
// #################################################################################################### //

int main ( int argc, const char* argv[] )
{
	// NAMESPACES : //
	using namespace LibReflexion;
	using namespace std;

// DISPLAY CURRENT SCOPE REFLEXIVE DATAS: ************************************************************* //
	cout << endl;

	cout << CurrentScope()->Description() << endl;

	cout << CurrentScope()->Name() << endl;

	for ( const string& memberName : CurrentScope()->ListMembers( true ) )
		cout << "\t" << memberName << endl;

	cout << endl;

// ACCESS REFLEXIVES VARIABLES : ********************************************************************** //
	
	cout << "/----------/ TESTING VARIABLES API \\----------\\" << endl;
	cout << endl;

	cout << "Direct variable access:" << endl;
	cout << "\t" << "globalVariable1 : " << globalVariable1 << endl;
	cout << "\t" << "globalVariable2 : " << globalVariable2 << endl;
	cout << endl;

	cout << "Reflexive variable access:" << endl;
	cout << "\t" << R_VAR( "LibTests::globalVariable1" ).Description() << endl;
	cout << "\t" << R_VAR( "LibTests::globalVariable2" ).Description() << endl;
	cout << endl;

	cout << "Changing variable values." << endl;
	R_VAR( "LibTests::globalVariable1" ) = 101;
	R_VAR( "LibTests::globalVariable2" ) = 206;
	cout << endl;

	cout << "Direct variable access:" << endl;
	cout << "\t" << "globalVariable1 : " << globalVariable1 << endl;
	cout << "\t" << "globalVariable2 : " << globalVariable2 << endl;
	cout << endl;

	cout << "Reflexive variable access:" << endl;
	cout << "\t" << R_VAR( "LibTests::globalVariable1" ).Description() << endl;
	cout << "\t" << R_VAR( "LibTests::globalVariable2" ).Description() << endl;
	cout << endl;

// ACCESS REFLEXIVES FUNCTIONS : ********************************************************************** //

	cout << "/----------/ TESTING FUNCTIONS API \\----------\\" << endl;
	cout << endl;

	cout << "Direct function addressing:" << endl;
	cout << "\t      add : 0x" << (long)&add       << endl;
	cout << "\tsubstract : 0x" << (long)&substract << endl;
	cout << endl;

	cout << "Direct function call:" << endl;
	cout << "\t      add( 3, 4 ) = " << add( 3, 4 )       << endl;
	cout << "\tsubstract( 3, 4 ) = " << substract( 3, 4 ) << endl;
	cout << endl;

	cout << "Reflexive function addressing:" << endl;
	cout << "\t" << R_FUN( "LibTests::add"       ).Description() << endl;
	cout << "\t" << R_FUN( "LibTests::substract" ).Description() << endl;
	cout << endl;

	cout << "Reflexive function call:" << endl;
	cout << "\t      add( 3, 4 ) = " << R_FUN( "LibTests::add"       ).Call< int, int, int >( 3, 4 ) << endl;
	cout << "\tsubstract( 3, 4 ) = " << R_FUN( "LibTests::substract" ).Call< int, int, int >( 3, 4 ) << endl;
	cout << endl;


	// Exit
	return 0;
}